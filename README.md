# README #

# poly-pack #

![polymer_black_192x192.png](https://bitbucket.org/repo/78E9kL/images/1419031639-polymer_black_192x192.png)

## ** poly-pack serves as a starter project for polymer beginners. ** ##

*** there is also a more advanced and opinionated starter kit, called [polymer-starter-kit](https://github.com/PolymerElements/polymer-starter-kit), ***


*** which comes with a ton of best practises (probably to much if you are new to this). ***


## Features ##

* ## A meaningful project structure ##
![folder structure english.png](https://bitbucket.org/repo/78E9kL/images/1450351966-folder%20structure%20english.png)

* ## Recommended webcomponents from the [polymer catalog](https://elements.polymer-project.org/) ##

* ## [Gulp](http://gulpjs.com/) production scripts ##

## installation ##

### what you'll need ###
* [npm](https://www.npmjs.com/)

[Download npm](https://nodejs.org/en/download/)

From now on, we let npm install everything for us.
To do so, use your favourite cli and type following commands:


* [Bower](http://bower.io/)
```
#!cmd
npm install -g bower

```

* [Gulp](http://gulpjs.com/)
```
#!cmd
npm install --save-dev gulp

```


## First steps ##
1 . open your favourite cli and *cd* to your poly-pack

2 . setup you **package.json** with this command:

```
#!cmd
npm init
```
*you will be asked a few questions, which most can be answered with the defaul answer (just hit enter)*

3 . setup you **bower.json** with this command:
```
#!cmd
bower init
```
*you will be asked a few questions, which most can be answered with the defaul answer again (just hit enter)*



### to Install packages ###
```
#!cmd
bower install <package>
```
Bower will install packages to bower_components/.

## gulp tasks ##
### for developement: ###
```
#!cmd
gulp serve
```
this will open your browser and load your dev/index.html while refreshing your browser when you change your code!
### for production: ###
```
#!cmd
gulp produce
```
this will produce a deployable codebase into the **dist** folder.