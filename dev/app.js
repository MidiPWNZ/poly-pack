// FEATURE DETECTION
// loads features if they are not natively supported
var webComponentsSupported = ('registerElement' in document && 'import' in
  document.createElement('link') && 'content' in document.createElement(
    'template'));

if (!webComponentsSupported)
{
  var script = document.createElement('script');
  script.async = true;
  script.scr = '../bower_components/webcomponents/webcomponents.js'
  script.onload = finishLazyLoading;
  document.head.appendChild(script);
}
else
{
  console.log('webcomponents polyfilled');
}

function finishLazyLoading()
{

};
