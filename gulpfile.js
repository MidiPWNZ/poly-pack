var gulp = require('gulp'),
  serve = require('gulp-serve'),
  hydrolysis = require('hydrolysis'),
  vulcanize = require('gulp-vulcanize'),
  watch = require('gulp-watch'),
  browserSync = require('browser-sync'),
  historyApiFallback = require('connect-history-api-fallback'),
  reload = browserSync.reload,
  polybuild = require('polybuild');

var bases = {
  dev: 'dev',
  dist: 'dist',
  baseComponent: 'glasscabinet/dev/components/your-main-component/your-main-component.html',
  distComponents: 'dist/components/your-main-component/'
}

// Watch files for changes & reload
gulp.task('serve', [], function ()
{
  browserSync(
  {
    port: 9001,
    notify: false,
    logPrefix: 'PSK',
    snippetOptions:
    {
      rule:
      {
        match: '<span id="browser-sync-binding"></span>',
        fn: function (snippet)
        {
          return snippet;
        }
      }
    },
    // Run as an https by uncommenting 'https: true'
    // Note: this uses an unsigned certificate which on first access
    //       will present a certificate warning in the browser.
    // https: true,
    server:
    {
      baseDir: ['dev'],
      middleware: [historyApiFallback()],
      routes:
      {
        '/bower_components': 'bower_components',
        '/components': 'components',
        '/dev': 'dev'
      }
    }
  });

  gulp.watch(['dev/**/*.html', 'bower_components/**/*.html',
    'components/**/*.html', 'components/**/*.css'], reload);
});

gulp.task('copy', function ()
{
  return gulp.src([
    'dev/index.html',
      'dev/app.js',
    'bower_components/webcomponentsjs/webcomponents.js'
  ],
    {
      base: 'dev'
    })
    .pipe(gulp.dest(bases.dist));
})

gulp.task('stream', function ()
{
  return gulp.src('css/**/*.css')
    .pipe(watch('css/**/*.css'))
    .pipe(gulp.dest('build'));
});

gulp.task('callback', function (cb)
{
  watch('css/**/*.css', function ()
  {
    gulp.src('css/**/*.css')
      .pipe(watch('css/**/*.css'))
      .on('end', cb);
  });
});

gulp.task('build', function ()
{
  return gulp.src(bases.baseComponent)
    .pipe(polybuild(
    {
      maximumCrush: true,
      suffix: 'build'
    }))
    .pipe(gulp.dest(bases.distComponents));
})


//Konkatinates all HTML imports to one HTML file.
gulp.task('vulcanize', function ()
{
  return gulp.src(bases.baseComponent)
    .pipe(vulcanize(
    {
      stripComments: true,
      inlineScripts: true,
      inlineCss: true
    }))
    .pipe(gulp.dest('dist/components/your-main-component/build/'));
});

gulp.task('produce', ['vulcanize', 'build', 'copy']);
